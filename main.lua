function love.load()
	-- Basic configurations and initial variables
	paddle_dist_from_edge = 8
	paddle_width = 12
	paddle_height = 96
	paddle_speed = 600
	ball_size = 12
	ball_vertical_speed = 200
	ball_horizontal_speed = 150
	paddle1points = 0
	paddle2points = 0
	
	-- Set window size and title
    love.resize(love.window.getMode()) -- call the function used when the window gets resized, and pass the current window size as parameters
	font = love.graphics.newFont(48)
	love.graphics.setFont(font)
	
	-- Paddle start positions calculated, vertically centered in screen.
	paddle1x = paddle_dist_from_edge
	paddle1y = (screen_height/2)-(paddle_height/2)
	paddle2x = screen_width - paddle_width - paddle_dist_from_edge
	paddle2y = (screen_height/2)-(paddle_height/2)
	
	-- Tells the paddles how far they can move before going off screen
	paddleupperlimit = 4
	paddlelowerlimit = screen_height - paddle_height - 4
	
	-- Sets ball start position and direction
	ballx = ((screen_width/4)*3)-(ball_size/2)
	bally = ((screen_height/4)*3)-(ball_size/2)
	balldirx = false -- false = left ; true = right
	balldiry = false -- false = up ; true = down
	
	-- set ball limits
	ballupperlimit = 0
	balllowerlimit = screen_height - ball_size
	
	-- load sounds
	SoundHitWall = love.audio.newSource("sounds/sfx_coin_single2.wav","static")
	SoundHitPaddle = love.audio.newSource("sounds/sfx_coin_single3.wav","static")
	SoundMiss = love.audio.newSource("sounds/sfx_sounds_damage1.wav","static")
end

function love.resize(w,h)
	-- variables that need updating when the window size changes
	-- mostly copied from love.load(). potential room for polishing
	screen_width = w
	screen_height = h
	paddle2x = screen_width - paddle_width - paddle_dist_from_edge
	paddlelowerlimit = screen_height - paddle_height - 4
	balllowerlimit = screen_height - ball_size
end

function love.update(dt)
	-- get keyboard input, adjust paddle as necessary, make sure paddle doesn't go off screen
	if paddle1y > paddleupperlimit then
		if love.keyboard.isDown("w") then paddle1y = paddle1y - (paddle_speed*dt) end
		if paddle1y < paddleupperlimit then paddle1y = paddleupperlimit end
	end
	if paddle1y < paddlelowerlimit then
		if love.keyboard.isDown("s") then paddle1y = paddle1y + (paddle_speed*dt) end
		if paddle1y > paddlelowerlimit then paddle1y = paddlelowerlimit end
	end
	if paddle2y > paddleupperlimit then
		if love.keyboard.isDown("up") then paddle2y = paddle2y - (paddle_speed*dt) end
		if paddle2y < paddleupperlimit then paddle2y = paddleupperlimit end
	end
	if paddle2y < paddlelowerlimit then
		if love.keyboard.isDown("down") then paddle2y = paddle2y + (paddle_speed*dt) end
		if paddle2y > paddlelowerlimit then paddle2y = paddlelowerlimit end
	end
	
	-- make the ball move in the direction it is destined to go
	if balldirx then
		ballx = ballx + (ball_horizontal_speed * dt)
	else
		ballx = ballx - (ball_horizontal_speed * dt)
	end
	if balldiry then
		bally = bally + (ball_vertical_speed * dt)
	else
		bally = bally - (ball_vertical_speed * dt)
	end
	
	-- make the ball turn tf around if it's trying to go up or down off the screen
	if bally <= ballupperlimit then
		balldiry = true
		love.audio.play(SoundHitWall)
	end
	if bally >= balllowerlimit then
		balldiry = false
		love.audio.play(SoundHitWall)
	end
	
	-- check if the ball hits the paddles, and if it does, change the direction
	if paddle1x < ballx and ballx < paddle1x + paddle_width then
		if paddle1y < bally + ball_size and bally < paddle1y + paddle_height then
			balldirx = true
			love.audio.play(SoundHitPaddle)
		end
	end
	if paddle2x < ballx + ball_size and ballx + ball_size < paddle2x + paddle_width then
		if paddle2y < bally + ball_size and bally < paddle2y + paddle_height then
			balldirx = false
			love.audio.play(SoundHitPaddle)
		end
	end
	
	-- check if ball leaves left or right side, and grant a point and reset if it does
	if ballx < 0-ball_size then
		-- add a point
		paddle2points = paddle2points + 1
		-- serve ball from left
		ballx = ((screen_width/4)*1)-(ball_size/2)
		bally = ((screen_height/4)*1)-(ball_size/2)
		balldirx = true -- false = left ; true = right
		balldiry = true -- false = up ; true = down
		-- reset paddle positions
		paddle1y=(screen_height/2)-(paddle_height/2)
		paddle2y=(screen_height/2)-(paddle_height/2)
		love.audio.play(SoundMiss)
	end
	if ballx > screen_width then
		-- add a point
		paddle1points = paddle1points + 1
		-- serve ball from right
		ballx = ((screen_width/4)*3)-(ball_size/2)
		bally = ((screen_height/4)*3)-(ball_size/2)
		balldirx = false -- false = left ; true = right
		balldiry = false -- false = up ; true = down
		-- reset paddle positions
		paddle1y=(screen_height/2)-(paddle_height/2)
		paddle2y=(screen_height/2)-(paddle_height/2)
		love.audio.play(SoundMiss)
	end
end

function love.draw()
	love.graphics.rectangle("fill",(screen_width/2),0,2,screen_height) -- dividing line
	love.graphics.rectangle("fill",paddle1x,paddle1y,paddle_width,paddle_height) -- left paddle
	love.graphics.rectangle("fill",paddle2x,paddle2y,paddle_width,paddle_height) -- right paddle
	love.graphics.rectangle("fill",ballx,bally,ball_size,ball_size) -- ball
	love.graphics.printf(paddle1points,paddle_dist_from_edge+paddle_width+16,16,screen_width/2,"left") -- left score
	love.graphics.printf(paddle2points,screen_width/2,16,(screen_width/2)-paddle_dist_from_edge-paddle_width-16,"right") -- right score
end
