# pong

My own version of Pong recreated in Love2D

I heard from a friend that a good way to practice game development is by recreating classic games from scratch, and each game you recreate gets harder and harder as you go.
This is the first functioning game I have created. It is a simple remake of the classic game Pong.
You can adjust parameters to resize the window, paddles, ball, speed, etc how you like it.
It took me about 2-4 hours to make it functional.
This is also counting me relearning lua as I knew only basic things about it and it had been a few years since I used it,
as well as figuring out on the love2d wiki which functions to use for what.

The next game I recreate will probably be asteroids. I may also come back to this game and polish things up to make it even closer to the original pong game.
(For example, in the original pong, the middle line is dashed, and in mine, it's solid)